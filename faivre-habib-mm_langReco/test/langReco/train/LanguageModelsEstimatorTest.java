package langReco.train;

import java.io.File;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import langModel.MyNgramCounts;
import langModel.NgramCounts;

/**
 * @author faivre habib
 *
 */
public class LanguageModelsEstimatorTest {
	
	/**
 	*  create a language model with words from texts in all languages and order 1.
 	*/
	@Test
	public void testCreateLmWordOrder1() {
		File dossier = new File("data/train/");
		File[] dossierList = dossier.listFiles();
		String trainPath = new String();
		String ngName = new String();
		for (File file : dossierList) {
			if (file.getName().contains("train-")) {
				NgramCounts ngm = new MyNgramCounts();
				trainPath = "data/train/" + file.getName();
				ngm.scanTextFile(trainPath, 1);
				ngName = file.getName().replace(".txt", ".lm");
				ngm.writeNgramCountFile("lm/unigram-100-" + ngName);
				System.out.println("Fichier : unigram-100-" + ngName + " OK");
			}
		} 
		System.out.println("Unigram OK");
	}
	
	/**
 	*  create a language model with words from texts in all languages and order 2.
 	*/
	@Test
	public void testCreateLmWordOrder2() {
		File dossier = new File("data/train/");
		File[] dossierList = dossier.listFiles();
		String trainPath = new String();
		String ngName = new String();
		for (File file : dossierList) {
			if (file.getName().contains("train-")) {
				NgramCounts ngm = new MyNgramCounts();
				trainPath = "data/train/" + file.getName();
				ngm.scanTextFile(trainPath, 2);
				ngName = file.getName().replace(".txt", ".lm");
				ngm.writeNgramCountFile("lm/bigram-100-" + ngName);
				System.out.println("Fichier : bigram-100-" + ngName + " OK");
			}
		}	
		System.out.println("Bigram OK");
	}

	/**
 	*  create a language model with words from texts in all languages and order 3.
 	*/
	@Test
	public void testCreateLmWordOrder3() {
		File dossier = new File("data/train/");
		File[] dossierList = dossier.listFiles();
		String trainPath = new String();
		String ngName = new String();
		for (File file : dossierList) {
			if (file.getName().contains("train-")) {
				NgramCounts ngm = new MyNgramCounts();
				trainPath = "data/train/" + file.getName();
				ngm.scanTextFile(trainPath, 3);
				ngName = file.getName().replace(".txt", ".lm");
				ngm.writeNgramCountFile("lm/trigram-100-" + ngName);
				System.out.println("Fichier : trigram-100-" + ngName + " OK");
			}
		}
		System.out.println("Trigram OK");
	}


	@Rule
	public TestName name = new TestName();

	
	@Before
	public void printSeparator()
	{
		System.out.println("\n=== " + name.getMethodName() + " =====================");
	}

}
