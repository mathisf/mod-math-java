package langReco.reco;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
public class Run2 {
	
	@Test
	public void testMyLanguageRecognizer2Unigram() {
		String goldSentPath = "data/gold/test-sent.txt";
		String configFile = "lm/fichConfig_unigram-100.txt";
		LanguageRecognizer myRecognizer = new MyLanguageRecognizer2(configFile);
		String hypLangFilePath = "data/test/test-lang-hyp1.txt";
		
		myRecognizer.recognizeFileLanguage(goldSentPath, hypLangFilePath);
	}
	
	@Rule
	public TestName name = new TestName();
	
	@Before
	public void printSeparator()
	{
		System.out.println("\n=== " + name.getMethodName() + " =====================");
	}
}
