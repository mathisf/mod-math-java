package langReco.reco;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
public class Run1 {
	
	@Test
	public void testMyLanguageRecognizer3Unigram() {
		String goldSentPath = "data/gold/test-sent.txt";
		String configFile = "lm/fichConfig_unigram-100.txt";
		LanguageRecognizer myRecognizer = new MyLanguageRecognizer3(configFile);
		String hypLangFilePath = "data/test/test-lang-hyp2.txt";
		
		myRecognizer.recognizeFileLanguage(goldSentPath, hypLangFilePath);
	}
	
	@Rule
	public TestName name = new TestName();
	
	@Before
	public void printSeparator()
	{
		System.out.println("\n=== " + name.getMethodName() + " =====================");
	}
}
