package langReco.reco;

import langReco.eval.Performance;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

public class MyLanguageRecognizer3Test {
	
	@Test
	public void testMyLanguageRecognizer3Unigram() {
		String goldSentPath = "data/gold/gold-sent.txt";
		String goldLangPath = "data/gold/gold-lang.txt";
		String configFile = "lm/fichConfig_unigram-100.txt";
		LanguageRecognizer myRecognizer = new MyLanguageRecognizer3(configFile);
		String hypLangFilePath = "tmp/hyp3Uni";
		
		myRecognizer.recognizeFileLanguage(goldSentPath, hypLangFilePath);
		System.out.printf("System performance = %f\n", Performance.evaluate(goldLangPath, hypLangFilePath));
	}
	
	@Test
	public void testMyLanguageRecognizer3Bigram() {
		String goldSentPath = "data/gold/gold-sent.txt";
		String goldLangPath = "data/gold/gold-lang.txt";
		String configFile = "lm/fichConfig_bigram-100.txt";
		LanguageRecognizer myRecognizer = new MyLanguageRecognizer3(configFile);
		String hypLangFilePath = "tmp/hyp3Bi";
		
		myRecognizer.recognizeFileLanguage(goldSentPath, hypLangFilePath);
		System.out.printf("System performance = %f\n", Performance.evaluate(goldLangPath, hypLangFilePath));
	}
	
	@Test
	public void testMyLanguageRecognizer3Trigram() {
		String goldSentPath = "data/gold/gold-sent.txt";
		String goldLangPath = "data/gold/gold-lang.txt";
		String configFile = "lm/fichConfig_trigram-100.txt";
		LanguageRecognizer myRecognizer = new MyLanguageRecognizer3(configFile);
		String hypLangFilePath = "tmp/hyp3Tri";
		
		myRecognizer.recognizeFileLanguage(goldSentPath, hypLangFilePath);
		System.out.printf("System performance = %f\n", Performance.evaluate(goldLangPath, hypLangFilePath));
	}
	
	@Rule
	public TestName name = new TestName();

	
	@Before
	public void printSeparator()
	{
		System.out.println("\n=== " + name.getMethodName() + " =====================");
	}
}
