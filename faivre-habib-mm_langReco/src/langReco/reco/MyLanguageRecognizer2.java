package langReco.reco;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import langModel.LanguageModel;
import langModel.MyLaplaceLanguageModel;
import langModel.MyNgramCounts;
/**
 * 
 * @author faivre habib
 * 
 * Language recognizer using Laplace language model and better recognizer
 *
 */
public class MyLanguageRecognizer2 extends LanguageRecognizer {
	protected Map<String,Map<String, LanguageModel>> lmModelMap;
	
	public MyLanguageRecognizer2(String fileParam){
		super();
		this.loadNgramCountPath4Lang(fileParam);
		this.lmModelMap = new HashMap<String,Map<String,LanguageModel>> ();
		this.lang = new ArrayList<String>(this.getLanguages());
		for (String lang : this.getLanguages()) {
			for (String ngName : this.getNgramCountNames(lang)) {
				MyNgramCounts ngTmp = new MyNgramCounts();
				LanguageModel lmTmp = new MyLaplaceLanguageModel();
				ngTmp.readNgramCountsFile(this.getNgramCountPath(lang, ngName));
				lmTmp.setNgramCounts(ngTmp);
				Map<String, LanguageModel> nameNgramMap = new HashMap<String, LanguageModel>();
				nameNgramMap.put(ngName, lmTmp);
				lmModelMap.put(lang, nameNgramMap);
			}
		}
	} 
	
	@Override
	public String recognizeSentenceLanguage(String sentence) {
		Map<String, Double> probs = new HashMap<String, Double>(); //Table de hachage des probabilité avec en clé les langues et en valeur les probabilitées
		String maxLang = "unk";
		double sum = 0.0; //somme des probabilitées
		double percentage;
		double prob;
		for (String language : this.lmModelMap.keySet()) {
			for (String ngName : this.lmModelMap.get(language).keySet()) {
				prob = this.lmModelMap.get(language).get(ngName).getSentenceProb(sentence);
				probs.put(language, prob);
			}
		}
		for (Double proba : probs.values()) {
			sum += proba;
		}
		for (String lang : probs.keySet()) {
			percentage = (probs.get(lang) * 100) / sum;
			if(percentage > 99.9999){
				maxLang = lang;
			}
		}
		return maxLang;
	}
}
