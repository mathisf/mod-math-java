package langReco.reco;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import langModel.LanguageModel;
import langModel.MyLaplaceLanguageModel;
import langModel.MyNgramCounts;

/**
 * 
 * @author faivre habib
 * 
 * Language recognizer using laplace language model
 *
 */
public class MyLanguageRecognizer3 extends LanguageRecognizer {
	protected Map<String,Map<String, LanguageModel>> lmModelMap;
	
	public MyLanguageRecognizer3(String fileParam){
		super();
		this.loadNgramCountPath4Lang(fileParam);
		this.lmModelMap = new HashMap<String,Map<String,LanguageModel>> ();
		this.lang = new ArrayList<String>(this.getLanguages());
		for (String lang : this.getLanguages()) {
			for (String ngName : this.getNgramCountNames(lang)) {
				MyNgramCounts ngTmp = new MyNgramCounts();
				LanguageModel lmTmp = new MyLaplaceLanguageModel();
				ngTmp.readNgramCountsFile(this.getNgramCountPath(lang, ngName));
				lmTmp.setNgramCounts(ngTmp);
				Map<String, LanguageModel> nameNgramMap = new HashMap<String, LanguageModel>();
				nameNgramMap.put(ngName, lmTmp);
				lmModelMap.put(lang, nameNgramMap);
			}
		}
	} 
	
	@Override
	public String recognizeSentenceLanguage(String sentence) {
		double maxProb = 0.0;
		String maxLang = "";
		double prob;
		for (String lang : this.lmModelMap.keySet()) {
			for (String ngramName : this.lmModelMap.get(lang).keySet()) {
				prob = this.lmModelMap.get(lang).get(ngramName).getSentenceProb(sentence);
				if (prob > maxProb) {
					maxProb = prob;
					maxLang = lang;
				}
			}
		}
		return maxLang;
	}

}
