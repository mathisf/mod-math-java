package langModel;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

public class MyNgramCountsTest {
	MyNgramCounts ngramcounts;

	@Before
	public void setUp() throws Exception {
		ngramcounts = new MyNgramCounts();
	}
	
	@Test
	public void testWriteNgramCountFile() {
		ngramcounts.scanTextFile("data/train/sample-en.txt", 3);
		ngramcounts.writeNgramCountFile("lm/trigram-100-train-en.lm");
	}

	@Test
	public void testReadNgramCountsFile() {
		ngramcounts.readNgramCountsFile("lm/trigram-100-train-en.lm");
		assertEquals(1, ngramcounts.getCounts("tree"));
		assertEquals(1, ngramcounts.getCounts("a tree"));
		assertEquals(3, ngramcounts.getMaximalOrder());
		assertEquals(27, ngramcounts.getTotalWordNumber());
	}

	@Test
	public void testscanTextFile() {
		ngramcounts.scanTextFile("data/train/sample-en.txt", 3);
		assertEquals(1, ngramcounts.getCounts("tree"));
		assertEquals(1, ngramcounts.getCounts("a tree"));
		assertEquals(3, ngramcounts.getMaximalOrder());
		assertEquals(27, ngramcounts.getTotalWordNumber());
	}
	
	@Rule
	public TestName name = new TestName();

	
	@Before
	public void printSeparator()
	{
		System.out.println("\n=== " + name.getMethodName() + " =====================");
	}

}
