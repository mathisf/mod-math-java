package langModel;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

public class MyLaplaceLanguageModelTest {
	NgramCounts ngramcounts;
	MyLaplaceLanguageModel naivelm;
	
	@Before
	public void setUp() throws Exception {
		ngramcounts = new MyNgramCounts();
		ngramcounts.readNgramCountsFile("lm/bigram-100-train-en.lm");
		naivelm = new MyLaplaceLanguageModel();
		naivelm.setNgramCounts(ngramcounts);
	}
	
	@Test
	public void testgetNGramProb() {
		System.out.println("tree -> proba : " + naivelm.getNgramProb("tree"));
		System.out.println("asdfasdfasasfdasdf -> proba : " + naivelm.getNgramProb("asdfasdfasasfdasdf"));
	}
	
	@Test
	public void testgetSentenceProb() {
		System.out.println("<s> i would have liked . </s> -> proba : "+naivelm.getSentenceProb("<s> i would have liked . </s>"));
		System.out.println("<s> This is not not not a sentence . </s> -> proba : "+naivelm.getSentenceProb("<s> This is not not not a sentence . </s>"));
	}
	
	@Rule
	public TestName name = new TestName();

	
	@Before
	public void printSeparator()
	{
		System.out.println("\n=== " + name.getMethodName() + " =====================");
	}
}
