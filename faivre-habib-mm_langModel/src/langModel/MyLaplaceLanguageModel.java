package langModel;


/**
 * Class MyLaplaceLanguageModel: class inheriting the class MyNaiveLanguageModel by creating 
 * a n-gram language model using a Laplace smoothing.
 * 
 * @author faivre habib
 *
 */
public class MyLaplaceLanguageModel extends MyNaiveLanguageModel {

	@Override
	public Double getNgramProb(String ngram) {
		int ngramSize = ngram.split(" ").length;
		double sum = 0.0;
		String historique = new String();
		double ngramProb = this.ngramCounts.getCounts(ngram) + 1; // lissage laplace
		
		if (ngramSize == 1) {
			return ngramProb / (this.ngramCounts.getTotalWordNumber() + this.vocabulary.getSize());
		}
		else if ( ngramSize < this.getLMOrder() ) {
				historique = NgramUtil.getHistory(ngram, ngramSize);
		} else {
				historique = NgramUtil.getHistory(ngram, this.getLMOrder());
		}
		for (int i = 0; i < this.vocabulary.getWords().size(); i++) {
			sum += this.ngramCounts.getCounts(historique);
		}
		return  ngramProb / (sum + this.vocabulary.getSize());
	}
}
